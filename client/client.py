import cv2 as cv
import numpy as np
from pybricks.messaging import BluetoothMailboxClient, TextMailbox
import math

callBackImg = None

tape_upper, tape_lower = [],[]
red_upper, red_lower = [],[]
green_upper, green_lower = [],[]
pink_upper, pink_lower = [],[]
black_upper, black_lower = [],[]
yellow_upper, yellow_lower = [],[]
brown_upper, brown_lower = [],[]

allowance = 10
count = 0

color_dict = {
    0 : (tape_upper, tape_lower),
    1 : (green_upper, green_lower),
    2 : (red_upper, red_lower),
    3 : (pink_upper, pink_lower),
    4 : (black_upper, black_lower),
    5 : (yellow_upper, yellow_lower),
    6 : (brown_upper, brown_lower)
}

def set_rgb(event, x, y, flags, params):
    # EVENT_MOUSEMOVE
    global count
    global color_dict
    if event == cv.EVENT_LBUTTONDOWN:
        print(str(x)+" "+str(y)+" "+str(callBackImg[y][x][2])+" "+str(callBackImg[y][x][1])+" "+str(callBackImg[y][x][0]))
        bounds = color_dict[count]
        for i in reversed(callBackImg[y][x]):
            up = i + allowance
            low = i - allowance
            if up > 255:
                up = 255
            if low < 0:
                low = 0
            bounds[0].append(up)
            bounds[1].append(low)
        color_dict[count] = bounds
        count += 1

def centroid(img):
    (height, width, channels) = img.shape
    x = []
    y = []
    for i in range(height):
        for j in range(width):
            if img[i][j] == 255:
                x.append(i)
                y.append(j)
    return (math.ceil(sum(x)/len(x)), math.ceil(sum(y)/len(y)))

def segment(img, upper, lower):
    #get the dimensions of the image
    (height, width, channels) = img.shape

    #convert to numpy array and flip from rgb to bgr
    upper = np.flip(np.array(upper))
    lower = np.flip(np.array(lower))

    #segment using Opencv inRange
    new_image = cv.inRange(img, lower, upper)

    #reshape to 3 dims
    return np.reshape(new_image, (new_image.shape[0],new_image.shape[1],1))

def load_image(path):
    return cv.imread(cv.samples.findFile(path))

def display_image(img):
    global callBackImg
    callBackImg = img
    cv.imshow("Display window", callBackImg)
    cv.setMouseCallback("Display window", set_rgb)
    while True:
        k = cv.waitKey(1)
        if k == ord('q'):
            break

SERVER = '24:71:89:4a:5c:59'

def client():
    client = BluetoothMailboxClient()
    mbox = TextMailbox('greeting', client)

    print('establishing connection...')
    client.connect(SERVER)
    print('connected!')
    while 1:
        cmd = input("Enter a cmd: ")
        if cmd =="buttons":
            msg = cmd + ":" + "dummy"
            print("Sending message " + msg)
            mbox.send(msg)
            mbox.wait() #wait for a response
            b = mbox.read()
            print("lasted button pressed was "+str(b))
        elif cmd == "run":
            distance = input("Enter a distance: ")
            msg = cmd + ":" + distance
            print("Sending message " + msg)
            mbox.send(msg)
        else:
            print("Unrecognized cmd: "+cmd)



green_center = () 
red_center = () 
yellow_center = ()  
pink_center = ()
black_center = ()  
brown_center = ()

def main():
    #Proof of conecept to make sure methods are working
    global green_center, red_center, yellow_center, black_center, pink_center, brown_center
    print("Set bounds in this order: Tape, green, red, orange, black, yellow")
    orig = cv.imread(cv.samples.findFile("test2.png"))
    display_image(orig)

    path = segment(orig, tape_upper, tape_lower)

    green_dot = segment(orig, green_upper, green_lower)
    green_center = centroid(green_dot)

    yellow_dot = segment(orig, yellow_upper, yellow_lower)
    yellow_center = centroid(yellow_dot)

    red_dot = segment(orig, red_upper, red_lower)
    red_center = centroid(red_dot)

    black_dot = segment(orig, black_upper, black_lower)
    black_center = centroid(black_dot)

    pink_dot = segment(orig, pink_upper, pink_lower)
    black_center = centroid(pink_dot)

    brown_dot = segment(orig, brown_upper, brown_lower)
    brown_center = centroid(brown_dot)
    
    #client()
    
main()